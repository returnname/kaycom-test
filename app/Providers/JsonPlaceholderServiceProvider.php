<?php
/**
 * Created by PhpStorm.
 * User: А-2017-11
 * Date: 05.03.2018
 * Time: 21:23
 */

namespace App\Providers;

use App\Services\JsonPlaceholder;
use Illuminate\Support\ServiceProvider;


class JsonPlaceholderServiceProvider extends ServiceProvider
{



    public function register(){
        $this->app->bind(JsonPlaceholder::class, function ($app) {
            return new JsonPlaceholder($this->app['redis']);
        });
    }


}