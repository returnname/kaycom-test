<?php


namespace App\Http\Controllers;

use App\Services\JsonPlaceholder;

class TestController extends Controller
{

    /**
     * TestController constructor.
     * @param JsonPlaceholder $service
     */
    public function __construct(JsonPlaceholder $service)
    {
        $this->service = $service;
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getItems($id = null){
        $items = $this->service->get('posts', $id);

        if ($items['valid']){
            return response()->json($items['data'],200);
        }

        return response()->json('no data',400);
    }
}