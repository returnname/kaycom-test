<?php

namespace App\Services;

use Illuminate\Redis\RedisManager;

class JsonPlaceholder
{
    protected $url = 'https://jsonplaceholder.typicode.com/';

    protected $errors;

    protected $code;

    protected $redis;

    /**
     * JsonPlaceholder constructor.
     * @param RedisManager $redis
     */
    public function __construct(RedisManager $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @param $type
     * @param null $qty
     * @return array
     */
    public function get($type, $qty = null){
        $key = $this->getKey($type, $qty);

        $remoteData =  $this->execute($key);

        if ($valid = $this->isValid()){
            $this->handleRedisStore($remoteData, $key);
        }

        return [ 'data'=>  $remoteData , 'valid' => $valid ];
    }

    /**
     * @param $data
     * @param $key
     */
    protected function handleRedisStore($data , $key){
        if (!$this->redis->exists($key)){
            $this->redis->set($key, json_encode($data));
            return;
        }
        $cache = $this->redis->get($key);

        $cache === $data ?? $this->redis->set($key, json_encode($data));
    }

    /**
     * @return bool
     */
    public function hasErrors(){
        return $this->errors !== null;
    }

    /**
     * @return bool
     */
    public function isValid(){
        return $this->code !== 404 && !$this->hasErrors();
    }

    /**
     * @param $type
     * @param $id
     * @return string
     */
    public function getKey($type, $id){
        $key  = $id ? '/' . $id : '';

        return $type . $key;
    }


    /**
     * @param string $key
     * @return array
     */
    public function execute(string $key){
        $url = $this->url . $key;

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 90,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_HEADER => 1,
            CURLINFO_HEADER_OUT => 1,
            CURLOPT_VERBOSE => 1,
        ]);

        $response = curl_exec($ch);

        if (curl_errno($ch)) $this->errors = curl_error($ch);

        $this->code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $body = substr($response, curl_getinfo($ch, CURLINFO_HEADER_SIZE));

        curl_close($ch);

        return json_decode($body);
    }
}